# Documentation d'installation de l'Hiopos Box



Ce guide vous permettra d'installer de manière simple et rapide votre Hiopos Box, permettant de faire le lien entre votre établissement et d'autres institutions telles que la Boîte Noire gouvernementale.



## Matériel fourni

Le kit contient entre autre:

-	Une Raspberry, petite carte électronique, comprenant des périphériques USB, Ethernet, micro USB (pour y brancher des adaptateurs HDMI) ainsi qu'une prise Jack, un port carte micro-SD et un port pour l'alimentation
-	Un câble d'alimentation
-	Une carte micro-SD
-	Un câble HDMI pour brancher votre Hiopos Box sur un écran
-	Une boîte de protection
-	Un câble USB-RS232 qui sera nécessaire pour communiquer avec la boîte noire



## Utilisation générale

Lorsque vous utilisez l'Hiopos Box, veillez à ce que votre carte micro-SD soit correctement inséré dans le port associé. En général, l'Hiopos Box démarre dès que l'alimentation est connectée à une prise secteur. Vous pouvez de plus avoir un retour graphique sur votre écran, dès lors que vous branchez votre câble HDMI. Si vous le désirez, pour plus de confort d'utilisation, vous pouvez également brancher une souris/clavier sur les ports USB de votre Box.

Il vous faudra également brancher un câble éthernet ou bien mettre en place le wifi sur la box afin que les scripts d'installation puissent tout configurer correctement et que le serveur puisse fonctionner.



Pour allumer votre Hiopos Box, il vous suffit simplement de brancher le câble d'alimentation sur une prise secteur. Le système démarre alors automatiquement.



Pour éteindre votre Hiopos Box, vous avez plusieurs possibilités:

-	Aller dans le menu en haut à gauche et cliquez sur `Shutdown...` (tout en bas du menu). Puis cliquez à nouveau sur `Shutdown` lorsque le menu contextuel apparaît.

![Éteindre une Hiopos Box par le menu](.doc_images/raspberry_shutdown.png)



- Cliquer en haut sur l'icône du programme `LXTerminal`: une console noire apparaît. Écrivez alors `shutdown now` pour éteindre la box.

  
  
  ![Hiopos Box LXTerminal](.doc_images/lx-terminal.jpg)



- Débrancher le câble d'alimentation (déconseillé car cela peut supprimer des données non sauvegardées)



## Installer votre Hiopos Box

Voici la partie détaillant comment installer une Hiopos Box et faire en sorte qu'elle soit complètement fonctionnel. Il y a ici 2 moyens possibles: l'installation via l'image d'une carte SD Hiopos Box, ou bien une installation complètement manuelle.



<span style="color:red; font-size: 25px;">**1 - Installation via une image carte SD Hiopox Box**</span>



### Étape 0 : Préparation d'une image carte SD

Si vous ne disposez pas d'une image carte SD et souhaitez en créer une, vous pouvez réaliser les étapes suivantes (attention, ces commandes nécessitent un système Linux et une certaine maîtrise des instructions bash) :

- Retirer la carte SD de l'Hiopos Box et insérez là dans l'adaptateur, puis Insérez ce dernier dans un PC.
- Créer sur votre système un dossier qui va recueillir l'image de la carte. Puis saisissez dans un terminal la commande `sudo fdisk -l`.  Essayez de déterminer le système qui correspond à la carte SD de votre Hiopos Box. La carte SD étant de 16GB, cela doit être relativement simple de trouver (ici dans l'image par exemple, le système `/dev/sdb`. La taille affichée est inférieure à 16GB du fait que la taille d'un système de stockage est toujours inférieure en réalité à sa valeur théorique).



![Hiopos Box - Préparation image SD : déterminer le nom de la carte SD dans le système](.doc_images/hiopos_box_fdisk.jpg)





- Déplacez-vous dans le répertoire que vous avez créé précédemment avec la commande `cd chemin/vers/votre/repertoire` puis copiez l'image de la carte dans votre répertoire avec l'instruction`sudo dd if=/dev/sdb of=./hiopox_box_image.img status=progress`.  Le paramètre if de la commande précédente doit évidemment être adapté en fonction du nom de la carte SD sur votre machine. Le paramètres `status=progress` permet d'avoir un aperçu de l'avancée de la copie (la commande dd n'étant de base pas très verbeuse). Le processus de copie prend quelques minutes. N'interrompez pas la commande.

- Lorsque la copie se termine, vous avez dans votre répertoire un fichier `hiopos_box_image.img` d'une taille de 16GB environ. Cette image pourra être utilisée à chaque fois que vous configurerez la carte SD d'une nouvelle Hiopos Box.

  

### Étape 1: Préparation d'une nouvelle carte SD

On part du principe que vous souhaitez configurer la carte SD d'une nouvelle Hiopos Box. Vous avez une image créée au préalable et stockée dans votre système. Dans cette étape, on va copier les données de l'image dans la nouvelle carte SD. Cela permettra par la suite de disposer d'une image système identique à celle stockée dans le fichier img. Les dépendances nécessaires à la bonne installation de l'Hiopos Box seront donc déjà installées, d'où un gain de temps significatif pour l'installation chez le client.

La préparation de la carte réalise à peu de choses près les mêmes instructions que l'étape 0 précédemment détaillée.

On va donc ici réaliser les étapes suivantes:

- Insérez la carte SD dans un adaptateur et branchez ce dernier sur votre PC. Déplacez-vous dans le répertoire contenant votre image via la commande `cd chemin/vers/votre/répertoire`.
- Tapez la commande `sudo fdisk -l`. Repérez le nom de votre carte SD dans votre système interne (je vais reprendre pour la suite le même nom de carte SD que celui de l'étape de préparation d'une image, soit /dev/sdb).
- Il est important avant d'instaurer l'image système de vérifier qu'aucune partition n'est montée dans le système. On lance pour cela la commande `sudo mount | grep sdb`. Si rien ne s'affiche, il n'y a rien à faire. Si certaines partitions sont affichées (par exemple /dev/sdb1 et /dev/sdb2), on peut démonter celles qui sont listées via l'instruction `sudo umount /dev/sdb1 /dev/sdb2`.
- Saisissez l'instruction `sudo dd if=./hiopos_box_image.img of=/dev/sdb bs=4M status=progress`. L'écriture de l'image sur la carte débute. Cette étape prend un peu de temps le temps d'écrire les 16GB de l'image dans la carte (entre 10 et 30 minutes). N'interrompez pas le processus sous peine de ne pas avoir l'image entièrement écrite sur la carte et qu'elle soit considérée comme corrompue.
- Lorsque l'écriture est terminée, votre carte est complètement configurée. Vous pouvez l'éjecter de votre machine, l'insérer dans une nouvelle Hiopos Box et démarrer. Le tout devrait directement être fonctionnel et les dépendances importantes telles que docker/docker-compose sont déjà installées. Vous devriez trouver sur le bureau de la Hiopos Box quelques scripts d'installation.



La préparation de la carte est terminée. La Hiopos Box est dorénavant prête à l'installation chez le client. Pour cette étape référez-vous à la partie d'installation manuelle, étape 3. Suivez alors les sections 2) Installation du serveur FDM et 3) Installation du serveur l'Hiopos Box. Comme toutes les dépendances sont déjà pré-installé en amont, l'installation ne prendra que quelques instants.



<span style="color:red; font-size: 25px;">**2 - Installation manuelle complète**</span>



Afin d’installer correctement votre Hiopos Box, vous devez vous assurer d’avoir une carte SD contenant un système d’exploitation fonctionnel. Le plus courant est Raspbian. Si votre carte de stockage contient déjà ce dernier, suivez les étapes d'installation qui apparaissent à l'écran et passer à l’étape 2 directement.



### Étape 1 : Installer Raspbian sur votre Hiopos Box

Si votre carte SD est vierge, alors vous allez devoir installer Raspbian avant de pouvoir faire quoi que ce soit avec votre Hiopos Box. Nous allons pour cela passer par un processus relativement simple, optimisé pour les débutants avec la technologie Raspberry et qui s'appelle `NOOBS ("New Out Of the Box System") `.



Vous allez devoir aller sur la page des images système [NOOBS](https://www.raspberrypi.org/downloads/noobs/) et télécharger le fichier Zip  `NOOBS` d'une taille d'environ 2GB. Une fois que ce fichier sera téléchargé, il faudra extraire son contenu sur la carte SD. Votre carte sera donc comme suivant:



![Configuration de Raspbian - contenu de la carte SD](.doc_images/hiopos_box_sd_card.png)



Votre carte SD est dorénavant correctement configurée. Vous pouvez la placer dans le port prévu à son effet sur la carte Raspberry, puis allumer le système. Ce dernier va régler quelques paramètres basiques puis vous afficher une fenêtre de sélection de systèmes d'exploitation. Sélectionnez l'option recommandée `Raspberry Pi OS Full` (ou généralement désignée sous le terme `Raspbian`). Puis cliquez sur installer afin de lancer le processus d'installation du logiciel (vous pouvez au préalable si vous le souhaitez configurer la langue du système en bas de l'écran).



![Configuration de Raspbian - NOOBS : choix des systèmes d'exploitation](.doc_images/hiopos_box_noobs.png)



Une barre de chargement apparaît alors. Veuillez patienter jusqu'au bout et ne pas éteindre le système. Cette étape prend généralement entre 5 et 10 minutes.



![Configuration de Raspbian - NOOBS : Installation d'une image système ](.doc_images/hiopos_box_noobs_install.png)



Lorsque le tout est installé, vous arrivez alors sur le bureau de votre Hiopos Box. Il reste quelques réglages à paramétrer, comme notamment le fait de vous connecter à un réseau Wi-Fi (nécessaire pour la suite de la mise en place de la Box). Passez l'étape d'installation des mises à jour (cela peut prendre énormément de temps et n'aura pas de conséquences sur la configuration de l'Hiopos Box si elle n'est pas faite). Continuez et laissez le système redémarrer afin de finaliser les réglages effectués.



### Étape 2 : Récupérer les scripts d'installation

Il vous faut dorénavant télécharger les scripts d'installation. Lancez une fenêtre `LXTerminal` (voyez la partie précédente pour voir comment ouvrir ce programme). Dans la console, saisissez les commandes suivantes:

```shell
cd Desktop
wget https://bitbucket.org/deusesprl/installation-scripts/get/master.zip -O scripts.zip
unzip -j scripts.zip
```

Cela va vous placer dans le dossier Desktop, télécharger une archive Zip du répertoire contenant tous les scripts nécessaires à la bonne installation de l'Hiopos Box et les extraire tous dans votre répertoire courant. Du fait que vous êtes placé dans le dossier Desktop, vous devriez donc les voir apparaître sur votre écran. Vous pouvez désormais si vous le désirez supprimer l'archive zip (manuellement ou bien par la commande `rm scripts.zip`).

Vous avez finalement une interface qui ressemble à:

![Configuration de Raspbian - ](.doc_images/hiopos_box_scripts.png)



L'interface de base de l'Hiopos Box est désormais correctement configuré. Vous allez pouvoir passer à l'étape d'exécution des scripts.



<span style="color:red; font-size: 25px;">**Attention ! Le fichier `install.sh` récupéré via cette méthode met en place un serveur de production. La boîte noire signera chaque transaction reçue et validée. **</span>



<span style="color:red; font-size: 25px;">**Pour effectuer de simples tests, récupérez le fichier de développement via l'url :**</span> 

```shell
wget https://bitbucket.org/deusesprl/installation-scripts/get/dev.zip -O scripts.zip
```

 <span style="color:red; font-size: 25px;">**et par l'extraction du fichier `install.sh` correspondant. La boîte noire sera alors en mode `TrainingSession` et  ne signera pas les transactions (le transactionNumber de ce fait ne s'incrémentera pas et sa valeur restera à 1).**</span>




### Étape 3 : Exécuter les scripts d'installation

L'installation d'une Hiopos Box a été conçu pour être le plus simple et le plus rapide possible. Après avoir démarré la box, vous vous retrouvez sur le bureau de votre box et vous pouvez alors voir plusieurs fichiers en dessous de la corbeille.



#### 1) Configuration de docker/docker-compose

Dans un premier temps, double-cliquez sur `docker_install.sh` et cliquez ensuite sur `Lancer dans un terminal` ou sur `Exécuter`. Le script va alors se lancer dans une console. Cette étape peut prendre un peu de temps, vu que le service télécharge des dépendances importantes pour la suite. N'éteignez pas ou n'arrêtez pas le processus. Si jamais une erreur apparaît à l'écran et que le processus s'interrompt subitement, cela peut être du à un problème de dépendance. Redémarrez la carte (tapez `reboot` dans la console ou sélectionnez l'option `Shutdown` dans le menu en haut à gauche puis `Reboot` dès lors que le menu contextuel est apparu). Puis lancez à nouveau le script, jusqu'à ce qu'une fenêtre graphique vous informe de la bonne configuration de la Box.

Lorsque cette première étape est terminée, une fenêtre apparaîtra et vous avertira du redémarrage du système. Appuyez sur OK et attendez que la box redémarre. Cela n'est en général qu'une affaire de quelques secondes.



![Configuration de la Hiopos Box - fenêtre de redémarrage](.doc_images/hiopos_box_reboot.png)



On peut maintenant passer à l'étape suivante : l'installation d'un serveur FDM qui va faire le lien entre l'Hiopos Box et la Boîte noire.



#### 2) Installation du serveur FDM

Exécutez le script `fdm_server.sh` en cliquant sur `Lancer dans un terminal`. Ce programme va télécharger des packages nécessaires à la bonne configuration du serveur. Puis 2 questions à la suite vont vous être demandées. Tapez `Y` aux 2 et une fenêtre va alors apparaître.



![Installation du serveur FDM - script et fenêtre des serveurs existants](.doc_images/hiopos_box_fdm_script.png)



Cliquez sur `Nouveau` et une seconde fenêtre apparaît, où vous pouvez nommer votre futur serveur. Saisissez `HioposBox` et cliquez sur OK sans rien changer dans les autres paramètres. Vous voyez désormais votre serveur, avec le statut `Démarré`. Connectez alors votre Boite Noire: le câble d'alimentation sur un ordinateur, ou via un adaptateur GSM, sur une prise secteur; le second câble sera connecté à l'adaptateur USB qui sera lui-même branché sur un des ports USB de l'Hiopos Box.

Lancez maintenant un navigateur internet et saisissez l'url `http://localhost:8858/`. Vous serez alors redirigé vers une page appelée `FirstUse`.



![Installation du serveur FDM - Navigateur : page première utilisation](.doc_images/hiopos_box_fdm_firstUse.png)



Si vous êtes dans une installation de test, sélectionnez la 3ème option (DEMO database). Sinon, sélectionnez l'option correspondant à la situation du restaurant et cliquez sur la flèche pour aller à la fenêtre suivante.

On va désormais vous demander le port de communication avec votre système. Si la Boite noire est correctement branchée, il devrait n'y avoir qu'une option disponible (avec USB dans son nom). Sélectionnez ce port et allez à la page suivante.



##### Cas 1 : Nouveau serveur sans base de données DEMO

Si vous avez sélectionné dans la toute première page la première option, cette page apparaît alors vous permettant de configurer les mots de passe des comptes de base du nouveau serveur.



![Installation du serveur FDM - Navigateur : page nouveau serveur](.doc_images/hiopos_box_fdm_newServer.png)



Après avoir rempli les informations, allez à la page suivante, qui vous demandera de saisir les informations légales de l'établissement.



![Installation du serveur FDM - Navigateur : page informations légales de l'établissement](.doc_images/hiopos_box_fdm_company.png)



Une fois ceci fait, votre nouveau serveur est entièrement configuré.



##### Cas 2 : Ajout d'un nouveau serveur

Si vous avez sélectionné l'option 2 dans la page FirstUse, vous allez devoir ajouter votre serveur à un environnement déjà existant. Vous devez saisir l'url d'un des serveurs déjà existants et les informations de connexion du compte administrateur ou support de ce serveur.



![Installation du serveur FDM - Navigateur : page d'ajout d'un serveur à un environnement déjà existant](.doc_images/hiopos_box_fdm_multiServer.png)



Une fois ceci fait, votre nouveau serveur est ajouté automatiquement à l'environnement et est directement configuré.



##### Cas 3 : Serveur de tests (avec base de données DEMO)

Dans ce cas là, il n'y a rien à faire. L'assistant de configuration va enregistrer les informations automatiquement avec des données fictives de tests.





Lorsque votre configuration de base est terminée, vous serez redirigé vers une page de connexion. Vous pouvez si vous le souhaitez vous connecter et vous atteindrez l'interface en lien avec la Boite Noire. Vous pourrez alors par la suite voir vos journaux et voir si les transactions qui ont été réalisé. L'installation du serveur FDM est désormais terminée. Il ne reste qu'un étape pour entièrement configurer votre Hiopos Box.



#### 3) Installation du serveur Hiopos Box

Exécutez le fichier `install.sh` puis sur `Lancer dans un terminal`. Sélectionnez votre langue et attendez quelques secondes. Une fenêtre apparaîtra alors et vous demandera l'identifiant fourni avec votre Hiopos Box. **Notez que l'identifiant ne compte que des chiffres et des lettres majuscules et/ou minuscules**. Si vous tapez un caractère interdit, une petite fenêtre vous indiquera une erreur et vous rappellera cette convention. Il faut également que l'identifiant saisi soit stocké dans notre base de données, ceci afin d'éviter toute erreur possible.

![Installation de la Hiopos Box - fenêtre de demande de l'ID](.doc_images/hiopos_box_id.png)



Lorsque ceci sera fait, le programme va chercher à vérifier la présence d'informations de configuration de la Hiopos Box correspondant à l'identifiant que vous avez saisi. S'il y en a, une fenêtre apparaîtra et vous demandera votre méthode d'installation (automatique/manuelle):



![Installation de la Hiopos Box - fenêtre de demande de méthode d'installation](.doc_images/hiopos_box_method.png)



##### Méthode d'installation automatique

Si vous avez coché oui sur la fenêtre précédente, vous souhaitez réaliser une installation automatique à partir des informations stockées dans notre base de données. Le programme va simplement récupérer ces données et finir l'installation rapidement sans plus vous demander d'intervenir dans le processus.



##### Méthode d'installation manuelle

Dans le cas où vous avez coché non dans la fenêtre précédente (ou si il n'y a pas de données liées à votre Hiopos Box dans notre base de données), vous allez configurer manuellement votre Hiopos Box.



Il va vous être demandé dans un premier temps de saisir votre identifiant de série du Point de Service (Point Of Service serial number). Cet identifiant est une chaîne de 14 caractères fourni par le SPF finance lors de la première installation d'une Boîte noire. Il est unique à chaque utilisateur et permettra par la suite d'identifier spécifiquement votre point de vente. De la même manière que pour l'identifiant Hiopos Box, une fenêtre d'erreur vous avertira si vous avez fait une erreur lors de la saisie de votre numéro (moins ou plus de 14 caractères saisi).



![Installation de la Hiopos Box - fenêtre de demande du numéro de série du Point de Service ](.doc_images/hiopos_box_serial_number.png)



Puis une seconde fenêtre apparaîtra par la suite pour vous demander avec quel service tiers vous souhaitez communiquer.

![Installation de la Hiopos Box - fenêtre de demande du service tiers](.doc_images/hiopos_box_service.png)



Sélectionnez votre service et validez votre option. **Si vous avez choisi le service Black Box** (communication directe avec la Boîte noire), il vous sera alors demandé vos identifiants de connexion Black Box. Si les identifiants saisis sont incorrects, une fenêtre vous invitera à recommencer.



![Installation de la Hiopos Box - fenêtre de demande des identifiants de connexion Black Box](.doc_images/hiopos_box_credentials.png)



Enfin, attendez encore un peu la fin de l'installation. Finalement, un message vous prévient que l'installation est terminée. Votre Hiopos Box est dores et déjà entièrement configuré et prête à l'emploi avec l'application Hiopos.



## Changer de service de communication tiers

Dans le cas où vous souhaiteriez changer de service avec lequel communiquer (ou si vous avez changé vos identifiants de connexion Black Box), vous pouvez à tout moment exécuter le script `external_service.sh` et sélectionner le nouveau service que vous souhaitez. Dans le cas du service Black Box, vos identifiants de connexion vous seront alors demandées. Un appel à notre serveur sera lancé et la modification sera effectuée dans notre base de données, ainsi que localement sur votre Hiopos Box. Si aucune erreur n'a été détecté, le système est directement opérationnel sur le nouveau service.