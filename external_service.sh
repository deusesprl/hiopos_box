#!/bin/bash

# Script to change the external communication service. For example, if an other service than BlackBox does a partnership with
# Hiopos, this script allows the user to change its service without re-installing the whole project.

# zenity (dialog window tool)
if ! [ -x "$(command -v zenity)" ]; then
  sudo apt-get --assume-yes install zenity
  sudo apt-get update
fi

# Global variables
# BASE_URL="http://hiopostoblackbox.local:8000/api"
BASE_URL="https://hiopostoblackbox.deuse.live/api"

LANGUAGE=$(zenity --entry --title="Change external service" --text="Select the language you prefer" \
  English Nederlands --entry-text="Français" --width=400 --height=200 2>/dev/null)

if [ $? -eq 1 ]
then
  zenity --error --title "Change external service" --text="Process canceled." --width=400 --height=200 2>/dev/null
  exit 1

elif [ "$LANGUAGE" = "English" ]
then
    MESSAGES=("Change external communication service" "You are going to exit the process. Are you sure you want to continue ?" "Process canceled"
    "Docker not installed: stopping the process. You must first execute the docker_install.sh file." 
    "Error with the Hiopos Box ID: your Hiopos Box is not known from our database. Please contact the support team." "Select the external service you want to communicate with" 
    "Please enter your Black Box credentials" "Username" "Password" "Error: your credentials are invalid. Please try again" "The change is now effective !")

elif [ "$LANGUAGE" = "Français" ]
then
    MESSAGES=("Changement du service de communication externe" "Vous allez quitter le processus. Êtes-vous sûr de vouloir continuer ?" "Processus arrêté"
    "Docker non installé: arrêt du processus. Vous devez d'abord exécuter le fichier docker_install.sh."
    "Erreur avec l'identifiant Hiopos Box: votre Hiopos Box est inconnu dans notre base de données. Veuillez contacter l'équipe de support."
    "Sélectionnez le service avec lequel vous souhaitez communiquer" "Veuillez saisir vos identifiants de connexion Boîte noire" "Nom d'utilisateur" 
    "Mot de passe" "Erreur: vos identifiants de connexion sont incorrects. Veuillez recommencer" "Le changement est dorénavant effectif !")

else
    MESSAGES=("Wijziging van de externe communicatiedienst" "U verlaat het installatieproces. Weet je zeker dat je wilt doorgaan ?" "Proces gestopt"
    "Docker niet geïnstalleerd: het proces stoppen. U moet eerst het docker_install.sh bestand uitvoeren."
    "Fout met de Hiopos Box ID: uw Hiopos Box is onbekend in onze database. Neem contact op met het ondersteuningsteam." "Selecteer de afdeling waarmee u wilt communiceren" 
    "Voer uw Black Box inloggegevens in" "Gebruikersnaam" "Wachtwoord" "Fout: uw inloggegevens zijn onjuist. Probeer het nog eens" "De verandering is nu effectief !")
fi

TITLE="${MESSAGES[0]}"
CONFIRM_CANCEL_MESSAGE="${MESSAGES[1]}"
CANCEL_MESSAGE="${MESSAGES[2]}"

# Check if docker and docker-compose commands are known
if ! [ -x "$(command -v docker)" ] || ! [ -x "$(command -v docker-compose)" ] ; then
    zenity --error --title "$TITLE" --text "${MESSAGES[3]}" --width=300 --height=200 2>/dev/null
    exit 1
fi

# Get the Hiopos Box local ID
ID_LINE=$(sed '3!d' ./service/settings.py)
LOCAL_ID="$(sed -e 's#.*= \(\)#\1#' <<< "$ID_LINE" | sed -e 's/^\"//' -e 's/\"$//')"

# Check if the local id is known from the database
ID_DB_STATUS="$(curl -s -o /dev/null -I -w "%{http_code}" "$BASE_URL/hiopos-box/$LOCAL_ID/box/")"
if ! [ "$ID_DB_STATUS" = "200" ]; then
    # Error DB: Hiopos Box ID unknown
    zenity --error --title "$TITLE" --text "${MESSAGES[4]}" --width=300 --height=200 2>/dev/null
    exit 1
fi

# Get external services from the DB
EXTERNAL_SERVICES="$(curl -s $BASE_URL/external-service/ | jq '.')"

# Get the length of the EXTERNAL_SERVICES object (number of services in the DB)
LENGTH="$(jq '. | length' <<< "$EXTERNAL_SERVICES")"

# Dynamically create an array with the name of all services
ARRAY=()
for (( i=0; i < LENGTH; ++i )) do
    OBJECT="$(jq --arg INDEX "$i" '.[$INDEX|tonumber]' <<< "$EXTERNAL_SERVICES")"
    NAME="$(jq '.name' <<< "$OBJECT" | sed -e 's/^\"//' -e 's/\"$//')"
    ARRAY+=("$NAME")
done

EXTERNAL_SERVICE=""
# Display a zenity window to let the user select the service it wants to communicate with
while [ -z "$EXTERNAL_SERVICE" ]; do
    EXTERNAL_SERVICE="$(zenity --list --title="$TITLE" --text="${MESSAGES[5]}" --column="" "${ARRAY[@]}" 2>/dev/null)"

    if [[ $? -eq 1 ]]; then
        # Cancel button pressed: we ask if the user wants to cancel or if it was an error
        zenity --question --title "$TITLE" --text "$CONFIRM_CANCEL_MESSAGE" --width=300 --height=200 2>/dev/null
        if [ $? = 0 ]; then
            # Yes button: the user confirms it wants to exit. Display the cancellation message and exit
            zenity --error --title "$TITLE" --text "$CANCEL_MESSAGE" --width=300 --height=200 2>/dev/null
            exit 1
        fi
    fi
done

# Get the ID of the selected external service
SELECTED_SERVICE_ID="$(jq --arg NAME "$EXTERNAL_SERVICE" '.[] | select(.name==$NAME) | .identifier' <<< "$EXTERNAL_SERVICES" | sed -e 's/^\"//' -e 's/\"$//')"

# Get the url of the selected external service
SELECTED_SERVICE_URL="$(jq --arg NAME "$EXTERNAL_SERVICE" '.[] | select(.name==$NAME) | .url' <<< "$EXTERNAL_SERVICES")"

# Get the eventual credentials parameter if it already exists
CREDENTIALS_LINE=$(sed '7!d' ./service/settings.py)
LOCAL_CREDENTIALS="$(sed -e 's#.*= \(\)#\1#' <<< "$CREDENTIALS_LINE" | sed -e 's/^\"//' -e 's/\"$//')"
if [ "$SELECTED_SERVICE_ID" = "BBX" ]
then
    # manage credentials validation
    CREDENTIALS_STATUS="401"
    while [ "$CREDENTIALS_STATUS" = "401" ]; do
        # get the credentials from the user
        CREDENTIALS="$(zenity --forms --title "$TITLE" --text "${MESSAGES[6]}" \
        --add-entry "${MESSAGES[7]}" \
        --add-password "${MESSAGES[8]}" \
        --separator "|" \
        2>/dev/null)"

        if [[ $? -eq 1 ]]; then
            # Cancel button pressed: we ask if the user wants to cancel or if it was an error
            zenity --question --title "$TITLE" --text "$CONFIRM_CANCEL_MESSAGE" --width=300 --height=200 2>/dev/null
            if [ $? = 0 ]; then
                # Yes button: the user confirms it wants to exit. Display the cancellation message and exit
                zenity --error --title "$TITLE" --text "$CANCEL_MESSAGE" --width=300 --height=200 2>/dev/null
                exit 1
            fi
        fi

        # Get the data entered and create the base64 string in order to do a request to the Black Box server
        USERNAME="$(echo -n "$CREDENTIALS" | cut -d "|" -f1 )" # username
        PASSWORD="$(echo -n "$CREDENTIALS" | cut -d "|" -f2 )" # password
        CREDENTIALS="$(echo -n  "$USERNAME:$PASSWORD" | base64)"

        # Request to the fdm server: if the return status code is 401, then the credentials are incorrect. Else, we can pass to the next step
        CREDENTIALS_STATUS="$(curl -s -o /dev/null -X POST -H "Authorization: Basic $CREDENTIALS" '{}' http://localhost:8858/client/HashAndSign/ | jq '.Errors[0].Code')"

        if [ "$CREDENTIALS_STATUS" = "401" ]
        then
            zenity --warning --title "$TITLE" --text "${MESSAGES[9]}" --width=300 --height=200 2>/dev/null
        else
            # Post the external service identifier and the credentials to the django server
            curl -s -X PUT -H "Content-Type: application/json" -d '{"external_service": "'"$SELECTED_SERVICE_ID"'", "username": "'"$USERNAME"'", "password": "'"$PASSWORD"'"}' "$BASE_URL/hiopos-box/$LOCAL_ID/update/" | jq '.'
        fi
    done

    # Store the credentials into settings file
    if [ -z "$LOCAL_CREDENTIALS" ]; then
        # If the hiopos Box doesn't contain the black Box credentials (first installation or first time using Black Box service...)
        # Add the credentials to the settings file
        printf "\nBLACK_BOX_CREDENTIALS = \"%s\"" "$CREDENTIALS" >> service/settings.py

    else
        # Check if the local credentials are the same as the current credentials or not
        # (the user may have changed its credentials since the last installation)
        if [ "$CREDENTIALS" != "$LOCAL_CREDENTIALS" ]; then
            # Replace the existing credentials by the new ones
            sed -i "s/$LOCAL_CREDENTIALS/$CREDENTIALS/g" service/settings.py
        fi
    fi


# Case where the external service isn't Black Box
else
    if [ -n "$LOCAL_CREDENTIALS" ]; then
        # If the Black Box credentials parameter exists in the settings file, we delete it (change of external service)
        sed -i '/BLACK_BOX_CREDENTIALS/d'  service/settings.py
        sed -i -Ez '$ s/\n+$//' service/settings.py
    fi
fi

# Get the Local external url
URL_LINE=$(sed '4!d' ./service/settings.py)
LOCAL_EXTERNAL_URL="$(sed -e 's#.*= \(\)#\1#' <<< "$URL_LINE")"

# Replace the EXTERNAL_URL variable into settings file
# (done here to be sure that if the user chose Black Box, he validated the credentials part)
sed -i "s,$LOCAL_EXTERNAL_URL,$SELECTED_SERVICE_URL,g" service/settings.py
sleep 1

# Recreate docker
docker-compose -f prod.yml build
docker-compose -f prod.yml up -d
zenity --info --title "$TITLE" --text "${MESSAGES[10]}" --width=400 --height=200 2>/dev/null
exit 0