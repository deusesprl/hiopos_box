import base64
import datetime
import fileinput
import json
import pprint
import sys
from logging.config import dictConfig

import requests
import xmltodict
from flask import Flask, abort, jsonify, make_response, request
from flask_restful import Api, Resource

import settings

# instantiate logging configuration
dictConfig(settings.LOGGING)

app = Flask(__name__)
api = Api(app)

# global variables through the app
pp = pprint.PrettyPrinter(indent=1)
logger = app.logger
cloud_license_app_id = None
cloud_license_auth_token = None
cloud_license_address = None
cloud_license_protocol = None

class Index(Resource):
   def get(self):
      return 'Hello world'


class FinalizeOrder(Resource):
   def post(self):
      arguments = request.json
      # pp.pprint(arguments)
      
      if arguments is None:
         logger.error(' \"[POST /finalizeOrder HTTP/1.1]\" - 400 - \"No arguments coming from Hiopos found\"')
         abort(400)

      # Parsing the xml data and create a json
      saleXMLParsed = json.loads(json.dumps(xmltodict.parse(arguments['saleXML'])))
      # pp.pprint(saleXMLParsed)

      saleResultXMLParsed = json.loads(json.dumps(xmltodict.parse(arguments['saleResultXML'])))
      
      finalJson = {}

      # Create a custon sale jsom thanks to a recursive loop other each object of the saleXMLParsed
      saleJson = self.createCustomJson(saleXMLParsed, {})

      # Create a custon saleResult jsom thanks to a recursive loop other each object of the saleResultXMLParsed
      saleResultJson = self.createCustomJson(saleResultXMLParsed, {})

      # add our customs json into the final object
      finalJson['saleData'] = saleJson
      finalJson['saleResultData'] = saleResultJson

      # add our own data
      finalJson['box_id'] = settings.HIOPOS_BOX_ID
      pp.pprint(finalJson)

      # Communication to the external service
      print('Sending json to external server...')

      if 'black-box' in settings.HIOPOS_BOX_SERVICE_URL:
         # Black Box case

         # Retreat the json to be accepted by the Black Box server
         blackbox_json = self.createBlackBoxJson(finalJson, {})
         pp.pprint(blackbox_json)

         # Build headers object
         headers = {
            "Accept-Language": "fr-BE",
            "Authorization": "Basic %s" %settings.BLACK_BOX_CREDENTIALS,
            "Content-Type": "application/json",
         }

         print(headers)

         # Send the Post request
         res = requests.post('http://{ip_address}:8858/client/HashAndSign/'.format(ip_address=settings.HIOPOS_BOX_IP_ADDRESS), headers=headers, json=blackbox_json)

      else:
         headers = {
            'Content-Type': 'application/json',
         }
         res = requests.post(settings.HIOPOS_BOX_SERVICE_URL, headers=headers, json=finalJson)
      
      print('\nresponse from server:\n')
      pp.pprint(res.json())

      if not res.ok:
         logger.error(' \"[POST /finalizeOrder HTTP/1.1]\" - %s - \"Error from external server\"' %(res.status_code))
         abort(res.status_code, description="Bad request or error from the external server.")

      if not app.debug:
         # Increment transaction number since the transaction is a success (only in prod mode)
         self.increment_transaction_number()
      
      # return success response and create the order in the Hiopos App
      return make_response(jsonify(success=True), 200)

   def _is_data_dict(self, data):
      return isinstance(data, dict) and '@Key' in data

   def createCustomJson(self, data, json):
      if self._is_data_dict(data):
         key = data.get('@Key')
         key = key[0].lower() + key[1:]
         return {key: data.get('#text', None)}

      if isinstance(data, dict):
         for key, value in data.items():
            if key is not None:
               key = key[0].lower() + key[1:]
               json[key] = self.createCustomJson(value, {})
         return json

      elif isinstance(data, list):
         if self._is_data_dict(data[0]):
            list_to_dict = {}
            for item in data:
               list_to_dict.update(self.createCustomJson(item, {}))
            return list_to_dict

         return [self.createCustomJson(item, {}) for item in data]

      return data


   def createBlackBoxJson(self, data, json):

      print('\n****** ENTER CREATE BLACK BOX JSON FUNCTION ******\n')

      document_json = data.get('saleData').get('document')

      header_json = document_json.get('header')
      headerfield_json = header_json.get('headerField')
      seller_json = header_json.get('seller').get('sellerField')

      json['POSSerialNumber'] = settings.HIOPOS_BOX_POS_SERIAL_NUMBER
      json['TerminalId'] = headerfield_json.get('posId', "")
      json['TerminalName'] = "Terminal %s" %(headerfield_json.get('posId', ""))
      json['OperatorId'] = 97 # Hiopos only on control terminals so 97 (according to the Black Box API doc)
      json['OperatorName'] = seller_json.get('name', "")

      # transactionNumber
      json['TransactionNumber'] = settings.transaction_number + 1

      date_time_str = "%s %s" %(headerfield_json.get('date', ""), headerfield_json.get('time', ""))
      date_time_obj = datetime.datetime.strptime(date_time_str, '%m-%d-%Y %H:%M:%S')
      # force date format to keep leading zeros
      date = "{d.year}-{d.month:02d}-{d.day:02d}T{d.hour:02d}:{d.minute:02d}:{d.second:02d}".format(d=date_time_obj)
      json['TransactionDateTime'] = date # date format YYYY-MM-DDTHH:MM:SS

      json['InvoiceNumber'] = ""
      json['IsTrainingMode'] = True if app.debug else False # depending on the running mode, the FDM module will eventually sign transactions
      json['IsFinalized'] = True
      json['IsRefund'] = False
      json['IsSimplifiedReceipt'] = False

      # Hiopos only uses control terminal, so no info about the drawer
      json['DrawerOpen'] = False
      json['DrawerId'] = ""
      json['DrawerName'] = ""

      # For now, info about the table where the command was taken from
      table_number = headerfield_json.get('tableNumber', None)
      json['Reference'] = "Command for table %s" % table_number if table_number else ""

      # Taxes
      taxes_json = document_json.get('taxes').get('tax').get('taxField')
      json['VatRateA'] = round(float(taxes_json.get('percentage', "21.00").replace(',', '.')), 2)
      json['VatRateB'] = 12 # No info about that in the XML data, we keep default data (specified in the Rest/JSON manual)
      json['VatRateC'] = 6
      json['VatRateD'] = 0

      # Product lines
      product_lines = []
      products = document_json.get('lines').get('line')
      if isinstance(products, list):
         # Array of objects
         for product in products:

            # Product line
            product_line = {}
            line_json = product.get('lineField', "")
            product_name = line_json.get('name', "")
            product_group_name = self.get_product_group_name(product_name) # get the product group name from the Cloud License server

            product_line['ProductGroupId'] = line_json.get('lineId', "")
            product_line['ProductGroupName'] = product_group_name
            product_line['ProductId'] = line_json.get('productId', "")
            product_line['ProductName'] = product_name
            product_line['Quantity'] = round(float(line_json.get('units', "0.00").replace(',','.')), 2)
            product_line['QuantityUnit'] = "P"
            product_line['SellingPrice'] = round(float(line_json.get('netAmount', "0.00").replace(',','.')), 2)
            product_line['VatRateId'] = product.get('lineTaxes').get('lineTax').get('lineTaxField').get('taxId', "")

            # Discount lines
            discount_lines = []
            discount_line = {}
            discount_line['DiscountId'] = line_json.get('discountReasonId', "")
            discount_line['DiscountName'] = product_group_name
            discount_line['DiscountType'] = "PRODUCTDISCOUNT"
            discount_line['DiscountGrouping'] = 0
            discount_line['DiscountAmount'] = round(float(line_json.get('discountAmount', "0.00").replace(',','.')), 2)

            discount_lines.append(discount_line)
            product_line['DiscountLines'] = discount_lines

            product_lines.append(product_line)
      else:
         # Object
         product_line = {}
         line_json = products.get('lineField')
         product_name = line_json.get('name',"")
         product_group_name = self.get_product_group_name(product_name) # get the product group name from the Cloud License server

         product_line['ProductGroupId'] = line_json.get('lineId', "")
         product_line['ProductGroupName'] = product_group_name
         product_line['ProductId'] = line_json.get('productId', "")
         product_line['ProductName'] = product_name
         product_line['Quantity'] = round(float(line_json.get('units', "0.00").replace(',', '.')), 2)
         product_line['QuantityUnit'] = "P"
         product_line['SellingPrice'] = round(float(line_json.get('netAmount', "0.00").replace(',','.')), 2)
         product_line['VatRateId'] = products.get('lineTaxes').get('lineTax').get('lineTaxField').get('taxId', "")

         # Discount lines
         discount_lines = []
         discount_line = {}
         discount_line['DiscountId'] = line_json.get('discountReasonId', "")
         discount_line['DiscountName'] = product_group_name
         discount_line['DiscountType'] = "PRODUCTDISCOUNT"
         discount_line['DiscountGrouping'] = 0
         discount_line['DiscountAmount'] = round(float(line_json.get('discountAmount', "0.00").replace(',','.')), 2)

         discount_lines.append(discount_line)
         product_line['DiscountLines'] = discount_lines

         product_lines.append(product_line)

      json['ProductLines'] = product_lines

      # Payment line
      payment_lines = []
      payment_means = document_json.get('paymentMeans').get('paymentMean')
      if isinstance(payment_means, list):
         # Array of objects
         for payment_mean in payment_means:

            # Payment line
            payment_line = {}
            payment_json = payment_mean.get('paymentMeanField')
            payment_line['PaymentId'] = payment_json.get('paymentMeanId')

            payment_mean_name = payment_json.get('paymenMeanName', "Other")
            payment_line['PaymentName'] = payment_mean_name
            if payment_mean_name == "Contant":
               payment_line['PaymentType'] = "CASH"
            elif payment_mean_name == "Credit Kaart":
               payment_line['PaymentType'] = "EFT"
            else:
               payment_line['PaymentType'] = "OTHER"

            payment_line['Quantity'] = int(payment_json.get('lineNumber', "1"))
            amount = round(float(payment_json.get('amount', "0.00").replace(',','.')), 2)
            payment_line['PayAmount'] = amount
            payment_line['Reference'] = ""

            currency = payment_json.get('currencyISOCode')
            if currency == "EUR":
               payment_line['ForeignCurrencyAmount'] = 0
               payment_line['ForeignCurrencyISO'] = ""
            else:
               payment_line['ForeignCurrencyAmount'] = round(amount * float(payment_json.get('currencyExchangeRate', "1").replace(',','.')), 2)
               payment_line['ForeignCurrencyISO'] = currency

            payment_lines.append(payment_line)
      else:
         # Object
         payment_line = {}
         payment_json = payment_means.get('paymentMeanField')
         payment_line['PaymentId'] = payment_json.get('paymentMeanId', "")

         payment_mean_name = payment_json.get('paymenMeanName', "")
         payment_line['PaymentName'] = payment_mean_name
         if payment_mean_name == "Contant":
            payment_line['PaymentType'] = "CASH"
         elif payment_mean_name == "Credit Kaart":
            payment_line['PaymentType'] = "EFT"
         else:
            payment_line['PaymentType'] = "OTHER"

         payment_line['Quantity'] = int(payment_json.get('lineNumber', "1"))
         amount = round(float(payment_json.get('amount', "0.00").replace(',','.')), 2)
         payment_line['PayAmount'] = amount
         payment_line['Reference'] = ""

         currency = payment_json.get('currencyISOCode', "")
         if currency == "EUR":
            payment_line['ForeignCurrencyAmount'] = 0
            payment_line['ForeignCurrencyISO'] = ""
         else:
            payment_line['ForeignCurrencyAmount'] = round(amount * float(payment_json.get('currencyExchangeRate', "1").replace(',','.')), 2)
            payment_line['ForeignCurrencyISO'] = currency

         payment_lines.append(payment_line)

      json['PaymentLines'] = payment_lines

      print('\n****** LEAVE CREATE BLACK BOX JSON FUNCTION ******\n')

      return json
   

   def get_product_group_name(self, product_name):
      """ Function to get the family product name from the cloud license """
      global cloud_license_app_id

      print('\n****** ENTER GET PRODUCT GROUP NAME FUNCTION ******\n')

      # initialize data when using for the first time
      if cloud_license_app_id is None:
         self.cloud_authenticate()

      # counter to avoid sending too much requests to the Cloud server
      attempts = 0
      while attempts < 2:
         url, headers, body = self.create_cloud_post_request(product_name)
         res = requests.post(url, headers=headers, json=body)
         if res.ok:
            rows = res.json().get('rows', [])
            pp.pprint(rows)
            print('\n****** LEAVE GET PRODUCT GROUP NAME FUNCTION ---> SUCCESS ******\n')
            return rows[0][3] if rows else 'Unknown family product'

         # if the user isn't autenticated or if the token delivered by the Cloud License has perished
         elif res.status_code == 401:
            logger.warning(' \"[POST /finalizeOrder HTTP/1.1]\" - %s - \"Reconnecting to Cloud License Server\"' %(res.status_code))
            self.cloud_authenticate()
            attempts += 1

      # too much attempts 
      print('\n****** LEAVE GET PRODUCT GROUP NAME FUNCTION ---> ERROR !!! ******\n')
      logger.warning(' \"[POST /finalizeOrder HTTP/1.1]\" - \"Can\'t retrieve family product name\"')
      return 'Unknown family product'


   def cloud_authenticate(self):
      """ Function to authenticate the user to the Cloud License
      1) We need the Deuse Cloud License credentials from the Django config server
      2) We login to the Cloud License platform, which will deliver us an authentication token
      3) We write all info to a data txt file to avoid authenticating for each product
      """
      global cloud_license_app_id
      global cloud_license_auth_token
      global cloud_license_address
      global cloud_license_protocol

      print('\n****** ENTER CLOUD AUTHENTICATE FUNCTION ******\n')

      # Get the Deuse Cloud License credentials
      res = requests.get('http://hiopostoblackbox.deuse.live/api/cloud-license/')
      if not res.ok:
         logger.error(' \"[POST /finalizeOrder HTTP/1.1]\" - %s - \"Can\'t get deuse credentials in hiopostoblackbox django server\"' %(res.status_code))
         abort(res.status_code)

      credentials = res.json()
      username = credentials.get('username', None)
      password = credentials.get('password', None)
      app_id = credentials.get('app_id', None)
      if credentials is None or password is None or app_id is None:
         logger.error(' \"[POST /finalizeOrder HTTP/1.1]\" - 400 - \"Empty Deuse Cloud License credentials\"')
         abort(400)      

      # Authenticate to the Cloud License server
      res = requests.get('https://cloudlicense.icg.eu/services/cloud/getCustomerWithAuthToken?email={u}&password={p}'
                     .format(u=username, p=password))
      if not res.ok:
         logger.error(' \"[POST /finalizeOrder HTTP/1.1]\" - %s - \"Impossible to authenticate to Cloud License server\"' %(res.status_code))
         abort(res.status_code)

      # get info from the Cloud server success response
      auth_data = json.loads(json.dumps(xmltodict.parse(res.content))).get('response', None).get('customerWithAuthTokenResponse', None)
      if auth_data is None:
         logger.error(' \"[POST /finalizeOrder HTTP/1.1]\" - 400 - \"Empty Cloud License authentication data\"')
         abort(400)
      
      # instantiate global cloud license variables
      cloud_license_app_id = app_id
      cloud_license_auth_token = auth_data.get('authToken', '')
      cloud_license_address = auth_data.get('address', '')
      cloud_license_protocol = auth_data.get('secure', True)

      print('\n****** LEAVE CLOUD AUTHENTICATE FUNCTION ******\n')

   
   def create_cloud_post_request(self, product_name):
      """ Function to create the Cloud License post request object info
      1) We create the headers. The Cloud License needs the auth token we got from the authentication function
         and we wrote into the data text file
      2) We create the json body file. We need for that the Cloud License application Id we also wrote in the
         data file
      3) We create the url string with the protocol (http or https) and the server addresss from the data file
      4) We finally return the 3 variables to be able then to do the post request
      """
      global cloud_license_app_id
      global cloud_license_auth_token
      global cloud_license_address
      global cloud_license_protocol

      print('\n****** ENTER CREATE CLOUD POST REQUEST FUNCTION ******\n')

      # Create post request info (headers/body/url)
      headers = {
         'Content-type': 'application/json',
         'x-auth-token': cloud_license_auth_token
      }
      pp.pprint(headers)

      body = {
         "appId": cloud_license_app_id,
         "exportationId": 1,
         "apiMode": 1,
         "filters": [
            {
                  "id": 1,
                  "conditions": [
                     {
                        "attributeId": 2,
                        "metricId": 0,
                        "logicOperator": "LIKE_PREFIX",
                        "attributeName": "Product",
                        "dimensionName": "Product",
                        "value": product_name,
                     }
                  ]
            }
         ]
      }
      pp.pprint(body)

      url = '{protocol}://{address}/ErpCloud/report/exportationExecute'.format(
         protocol = 'https' if cloud_license_address else 'http',
         address = cloud_license_address
      )
      print(url)

      print('\n****** LEAVE CREATE CLOUD POST REQUEST FUNCTION ******\n')
      return url, headers, body
   
   def increment_transaction_number(self):
      """ Method to increment the transaction number and overwrite the variable into the
      settings file. A cron is then configured to do a PUT request to the Django server in
      order to save this parameter. This function is called only on production mode since the
      Black Box will sign the transaction
      """
      settings.transaction_number += 1

      # Save the transaction_number value into settings file for security/storage purpose
      file = "settings.py"
      for line in fileinput.input([file], inplace=True):
         if line.strip().startswith('transaction_number = '):
            line = 'transaction_number = ' + str(settings.transaction_number) + '\n'
         sys.stdout.write(line)


   def get_product_group_name_static(self, product_id):
      """ Static method to get the product group name
      We need for that the product identifier from the Hiopos DB. If the Id isn't recognised,
      we return an unknown product group string
      """
      switcher= {
         range(2,13): 'Arroces',
         range(13, 35): 'Bocadillos Calientes',
         range(35, 49): 'Bocadillos Fríos',
         range(49, 69): 'Cafés e Infusiones',
         range(69, 103): 'Carnes',
         range(103, 118): 'Cervezas',
         range(120, 137): 'Ensaladas',
         range(137, 163): 'Entrantes',
         range(168, 178): 'Licores',
         range(178, 196): 'Pastas',
         range(196, 227): 'Pescados',
         range(227, 242): 'Platos combinados',
         range(242, 280): 'Postres',
         range(280, 316): 'Refrescos',
         range(316, 347): 'Tapas New',
         range(347, 386): 'Vinos i Cavas',
         range(390, 392): 'Menús'
      }

      # create a dict object from the switcher items
      switch = {key: value for range, value in switcher.items() for key in range}
      return switch.get(product_id, 'Unknown product group')


class TestExternalServer(Resource):
   def post(self):
      data = request.json
      print('Data received in external server:\n')
      pp.pprint(data)
      if data is None:
         logger.error(' \"[POST /finalizeOrder HTTP/1.1]\" - 400 - \"Error external server Test: empty or invalid data format provided\"')
         abort(400, description="The provided data isn't a valid json")
      return make_response(jsonify(success=True), 200)


api.add_resource(Index, '/')
api.add_resource(FinalizeOrder, '/finalizeOrder')
api.add_resource(TestExternalServer, '/resto-concept')

if __name__ == '__main__':
   app.run(debug=False, host='0.0.0.0', port='8000', use_reloader=False) # use_reloader is set to False to avoid reload on Cloud authentication data file runtime changes
