from logging.config import dictConfig

import requests
from flask import Flask

import settings

# instantiate logging configuration
dictConfig(settings.LOGGING)
app = Flask('__main__')
logger = app.logger

url = 'https://hiopostoblackbox.deuse.live/api/hiopos-box/{identifier}/update/'.format(identifier=settings.HIOPOS_BOX_ID)
res = requests.put(url, json={'transaction_number': settings.transaction_number})

if not res.ok:
    logger.error(' \"[PUT %s HTTP/1.1]\" - %s - \"Error during transaction number storage in django backup server\"' %(url, res.status_code))
else:
    logger.info(' \"[PUT %s HTTP/1.1]\" - %s - \"Transaction number storage in django backup server\"' %(url, res.status_code))
